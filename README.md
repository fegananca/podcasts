# The App

This is a web page that retrieves and display a list of the 100 best podcasts ranked by Apple.

The app consists of three  pages:

1. Main page: a list of podcasts with infinite scroll.
2. Podcast detail page: display detailed information about a selected podcast and its episodes list.
3. Episode detail page: display detailed information about a selected episode and its audio.

The app was built using React and styled using pure CSS.


## Getting started

In the project directory, you should run:

### `npm install`
Install all dependencies.

### `npm start`
Runs the app in development mode.
Open [http://localhost:3000] to view in your browser.

### `npm run build`
Runs the app in production mode.

### `npm run test`
Launches the test runner in the interactive watch mode.
