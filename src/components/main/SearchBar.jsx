import React from "react";
import { getLocalDataList } from "../../services/localStorage";

const SearchBar = ({ setPodcastList, setSearchQuery, counter, setCounter }) => {
  const onSearchData = (e) => {
    const localData = getLocalDataList();
    const filteredData = localData.filter((item) => {
      return (
        item["im:name"].label
          .toLowerCase()
          .includes(e.target.value.toLowerCase()) ||
        item["im:artist"].label
          .toLowerCase()
          .includes(e.target.value.toLowerCase())
      );
    });
    if (filteredData) {
      setSearchQuery(true);
    }
    setPodcastList(filteredData);
    setCounter(filteredData.length);
  };

  return (
    <div className="searchBar-container">
      <p>
        <span data-testid="counter">{counter}</span>
      </p>
      <input
        type="text"
        placeholder="Filter podcasts..."
        onChange={onSearchData}
      />
    </div>
  );
};

export default SearchBar;
