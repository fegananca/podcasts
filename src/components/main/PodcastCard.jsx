import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getPodcasts } from "../../services/api";
import {
  getListExpiration,
  getLocalDataList,
  removeListExpirationn,
  removeLocalDataList,
  storeList,
  storeListExpiration,
} from "../../services/localStorage";
import InfiniteScroll from "react-infinite-scroll-component";

const PodcastCard = ({
  podcastList,
  setPodcastList,
  searchQuery,
  setCounter,
  setisLoadingList,
}) => {
  const [hasMore, setHasMore] = useState(true);

  const fetchMoreData = () => {
    const localData = getLocalDataList();
    const nextData = localData.slice(
      podcastList.length,
      podcastList.length + 10
    );
    setPodcastList([...podcastList, ...nextData]);

    if (podcastList.length >= localData.length) {
      setHasMore(false);
    }
  };

  useEffect(() => {
    const fetchPodcasts = async () => {
      const localData = getLocalDataList();
      const listExpiration = getListExpiration();
      if (listExpiration && new Date().getTime() > listExpiration) {
        removeLocalDataList();
        removeListExpirationn();
      }
      if (localData) {
        setPodcastList(localData.slice(0, 10));
        setCounter(localData.length);
        setisLoadingList(false);
      } else {
        const expirationTime = new Date().getTime() + 24 * 60 * 60 * 1000;
        const data = await getPodcasts();
        storeList(data);
        storeListExpiration(expirationTime);
        setPodcastList(data.slice(0, 10));
        setCounter(data.length);
        setisLoadingList(false);
      }
    };
    fetchPodcasts();
  }, [setPodcastList, setCounter, setisLoadingList]);

  return (
    <>
      <InfiniteScroll
        dataLength={podcastList.length}
        next={!searchQuery && fetchMoreData}
        hasMore={hasMore}
        loader={!searchQuery && <h4 id="loadingMessage">Loading...</h4>}
        endMessage={<p id="endMessage">No more to see</p>}
      >
        <div className="grid">
          {podcastList.map((podcast) => {
            return (
              <Link
                to={`/podcast/${podcast.id.attributes["im:id"]}`}
                state={{ data: podcast }}
                key={podcast.id.attributes["im:id"]}
              >
                <div className="card-container">
                  <img
                    src={podcast["im:image"][2].label}
                    alt="podcast of the artist"
                  ></img>
                  <div className="card-info">
                    <h3>{podcast["im:name"].label}</h3>
                    <p>Author: {podcast["im:artist"].label}</p>
                  </div>
                </div>
              </Link>
            );
          })}
        </div>
      </InfiniteScroll>
    </>
  );
};

export default PodcastCard;
