import { render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter as Router } from "react-router-dom";
import mockData from "../mockData.json";
import * as localData from "../services/localStorage";
import PodcastCardDetail from "./PodcastCardDetail";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    state: { data: mockData.episodes.results[1] },
    pathname: `/podcast/${mockData.podcasts[0].id.attributes["im:id"]}`,
  }),
}));
jest.mock("../services/localStorage", () => ({
  getLocalDataList: jest.fn(),
  storeList: jest.fn(),
}));

describe("podcast detail page", () => {
  beforeEach(() => jest.clearAllMocks());
  test("should render episodes list", async () => {
    localData.getLocalDataList.mockImplementation(() => mockData.podcasts);

    render(
      <Router>
        <PodcastCardDetail
          selectedId={mockData.podcasts[0].id.attributes["im:id"]}
        />
      </Router>
    );
    await waitFor(() => {
      expect(
        screen.getByText(mockData.podcasts[0]["im:name"].label)
      ).toBeInTheDocument();
    });
  });
});
