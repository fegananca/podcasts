import React from "react";

const EpisodeCardDetail = ({ selectedEpisode }) => {
  return (
    <div className="episode-info">
      <h3>{selectedEpisode.trackName}</h3>
      <p>{selectedEpisode.description}</p>
      <div
        dangerouslySetInnerHTML={{ __html: selectedEpisode.shortDescription }}
      ></div>
      <audio
        controls
        src={selectedEpisode.episodeUrl}
        data-testid="audio"
      ></audio>
    </div>
  );
};

export default EpisodeCardDetail;
