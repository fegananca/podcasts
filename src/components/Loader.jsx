import React from "react";
import "../styles/Loader.css";
import { Icon } from "@iconify/react";

const Loader = () => {
  return (
    <div className="loader">
      <Icon icon="bx:current-location" color="#3383c6" width="25" />
    </div>
  );
};

export default Loader;
