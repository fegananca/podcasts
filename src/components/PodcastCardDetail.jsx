import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { getLocalDataList } from "../services/localStorage";

const PodcastCardDetail = ({ selectedId }) => {
  const [selectedPodcast, setSelectedPodcast] = useState("");
  const location = useLocation();

  const isEpPathname = location.pathname.startsWith(
    `/podcast/${selectedId}/episode/`
  );

  useEffect(() => {
    const fetchSelectedPodcast = async () => {
      const localData = getLocalDataList();
      if (localData) {
        const findPodcast = localData.find((podcast) => {
          return podcast.id.attributes["im:id"] === selectedId;
        });
        setSelectedPodcast(findPodcast);
      }
    };
    fetchSelectedPodcast();
  }, [selectedId]);

  return (
    selectedPodcast && (
      <div className="podcastCard-detail">
        <figure>
          <Link
            to={isEpPathname ? `/podcast/${selectedId}` : "#"}
            state={{ data: selectedPodcast }}
          >
            <img src={selectedPodcast["im:image"][2].label} alt="cover"></img>
          </Link>
        </figure>
        <div className="detailBox-1">
          <Link
            to={isEpPathname ? `/podcast/${selectedId}` : "#"}
            state={{ data: selectedPodcast }}
          >
            <h3>{selectedPodcast["im:name"].label}</h3>
            <p>by {selectedPodcast["im:artist"].label}</p>
          </Link>
        </div>
        <div className="detailBox-2">
          <h3>Description:</h3>
          <p>{selectedPodcast.summary.label}</p>
        </div>
      </div>
    )
  );
};

export default PodcastCardDetail;
