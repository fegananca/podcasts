import React from "react";
import "../styles/Header.css";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <div className="header-container">
        <Link to="/">Podcaster</Link>
      </div>
    </header>
  );
};

export default Header;
