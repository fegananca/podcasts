import React, { useEffect } from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import { getEpisodes } from "../../services/api";
import { formatDuration } from "../../services/utils";
import {
  getEpisodesExpiration,
  getLocalDataEpisodes,
  removeEpisodesExpiration,
  removeLocalDataEpisodes,
  storeEpisodes,
  storeEpisodesExpiration,
} from "../../services/localStorage";

const EpisodesList = ({
  artistId,
  episodesList,
  setEpisodesList,
  setisLoadingEp,
}) => {
  useEffect(() => {
    const fetchEpisodes = async () => {
      const localData = getLocalDataEpisodes(artistId);
      const episodesExpiration = getEpisodesExpiration(artistId);

      if (episodesExpiration && new Date().getTime() > episodesExpiration) {
        removeLocalDataEpisodes(artistId);
        removeEpisodesExpiration(artistId);
      }
      if (localData) {
        setEpisodesList(localData.slice(1));
        setisLoadingEp(false);
      } else {
        const dataAPI = await getEpisodes(artistId);
        const expirationTime = new Date().getTime() + 24 * 60 * 60 * 1000;
        storeEpisodes(dataAPI.results, artistId);
        storeEpisodesExpiration(expirationTime, artistId);
        setEpisodesList(dataAPI.results.slice(1));
        setisLoadingEp(false);
      }
    };
    fetchEpisodes();
  }, [artistId, setEpisodesList, setisLoadingEp]);

  return (
    <>
      <aside>
        <div className="episodes-counter" data-testid="ep-counter">
          Episodes:{episodesList.length}
        </div>
        <div className="table-container">
          <table>
            <thead>
              <tr>
                <th key="1">Title</th>
                <th key="2">Date</th>
                <th key="3">Duration</th>
              </tr>
            </thead>
            <tbody>
              {episodesList.map((episode) => {
                return (
                  <tr key={episode.trackId}>
                    <td>
                      <Link
                        to={`/podcast/${artistId}/episode/${episode.trackId}`}
                        state={{ data: episode }}
                      >
                        {episode.trackName}
                      </Link>
                    </td>
                    <td>
                      {moment.utc(episode.releasedDate).format("D/M/YYYY")}
                    </td>
                    <td>{formatDuration(episode.trackTimeMillis)}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </aside>
    </>
  );
};

export default EpisodesList;
