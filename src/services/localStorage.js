//Podcast list
export const getLocalDataList = () => {
  return JSON.parse(localStorage.getItem("podcasts"));
};

export const getListExpiration = () => {
  return JSON.parse(localStorage.getItem("podcastsExpiration"));
};

export const removeLocalDataList = () => {
  return localStorage.removeItem("podcasts");
};

export const removeListExpirationn = () => {
  return localStorage.removeItem("podcastsExpiration");
};

export const storeList = (data) => {
  return localStorage.setItem("podcasts", JSON.stringify(data));
};

export const storeListExpiration = (data) => {
  return localStorage.setItem("podcastsExpiration", JSON.stringify(data));
};

//Episodes
export const getLocalDataEpisodes = (id) => {
  return JSON.parse(localStorage.getItem(`episodes${id}`));
};

export const getEpisodesExpiration = (id) => {
  return JSON.parse(localStorage.getItem(`episodesExpiration${id}`));
};

export const removeLocalDataEpisodes = (id) => {
  return localStorage.removeItem(`episodes${id}`);
};

export const removeEpisodesExpiration = (id) => {
  return localStorage.removeItem(`episodesExpiration${id}`);
};

export const storeEpisodes = (data, id) => {
  return localStorage.setItem(`episodes${id}`, JSON.stringify(data));
};

export const storeEpisodesExpiration = (data, id) => {
  return localStorage.setItem(`episodesExpiration${id}`, JSON.stringify(data));
};
