export const getPodcasts = async () => {
  try {
    const podcastUrl =
      "https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json";
    const url = encodeURIComponent(`${podcastUrl}`);
    const response = await fetch(`https://api.allorigins.win/raw?url=${url}`);
    const data = await response.json();
    return data.feed.entry;
  } catch (err) {
    console.log(err);
  }
};

export const getEpisodes = async (id) => {
  try {
    const epidodesUrl = `https://itunes.apple.com/lookup?id=${id}&media=podcast&entity=podcastEpisode`;
    const url = encodeURIComponent(`${epidodesUrl}`);
    const response = await fetch(`https://api.allorigins.win/raw?url=${url}`);
    const data = await response.json();
    return data;
  } catch (err) {
    console.log(err);
  }
};
