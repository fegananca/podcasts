import { render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter as Router } from "react-router-dom";
import { useParams } from "react-router";
import mockData from "../mockData.json";
import * as api from "../services/api";
import PodcastDetail from "./PodcastDetail";

jest.mock("../services/api", () => ({
  getPodcasts: jest.fn(),
  getEpisodes: jest.fn(),
}));

jest.mock("react-router", () => ({
  ...jest.requireActual("react-router"),
  useParams: jest.fn(),
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    state: { data: mockData.podcasts[0] },
    pathname: `/podcast/${mockData.podcasts[0].id.attributes["im:id"]}`,
  }),
}));

describe("podcast detail page", () => {
  beforeEach(() => jest.clearAllMocks());

  test("should render episodes list", async () => {
    useParams.mockReturnValue({
      id: mockData.podcasts[0].id.attributes["im:id"],
    });
    api.getPodcasts.mockResolvedValue(mockData.podcasts);
    api.getEpisodes.mockResolvedValue(mockData.episodes);
    render(
      <Router>
        <PodcastDetail />
      </Router>
    );

    await waitFor(() => {
      expect(
        screen.getByText(mockData.episodes.results[1].trackName)
      ).toBeInTheDocument();
    });

    expect(screen.getByTestId("ep-counter")).toHaveTextContent(
      `Episodes:${mockData.episodes.results.length - 1}`
    );
  });
});
