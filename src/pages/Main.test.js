import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter as Router } from "react-router-dom";
import mockData from "../mockData.json";
import * as api from "../services/api";
import Main from "./Main";

jest.mock("../services/api", () => ({
  getPodcasts: jest.fn(),
  getEpisodes: jest.fn(),
}));

describe("main page", () => {
  beforeEach(() => jest.clearAllMocks());

  test("should render podcast list", async () => {
    api.getPodcasts.mockResolvedValue(mockData.podcasts);
    render(
      <Router>
        <Main />
      </Router>
    );

    await waitFor(() => {
      screen.getByText("The Joe Budden Podcast");
    });
  });

  test("should render total number of items in the screen", async () => {
    const totalItems = mockData.length;
    api.getPodcasts.mockResolvedValue(mockData.podcasts);
    render(
      <Router>
        <Main />
      </Router>
    );
    await waitFor(() => {
      const spanElement = screen.getByTestId("counter");
      expect(spanElement).toBeInTheDocument(totalItems);
    });
  });

  test("should display total number of items after search", async () => {
    api.getPodcasts.mockResolvedValue(mockData.podcasts);
    render(
      <Router>
        <Main />
      </Router>
    );
    const searchBar = screen.getByPlaceholderText("Filter podcasts...");
    const totalItems = screen.getByTestId("counter");

    fireEvent.change(searchBar, { target: { value: "Joe" } });
    fireEvent.submit(searchBar);

    expect(totalItems).toHaveTextContent("1");
  });
});
