import React from "react";
import "../styles/EpisodeDetail.css";
import { useLocation, useParams } from "react-router-dom";
import EpisodeCardDetail from "../components/episodes/EpisodeCardDetail";
import Header from "../components/Header";
import PodcastCardDetail from "../components/PodcastCardDetail";

const EpisodeDetail = () => {
  const { id } = useParams();
  const location = useLocation();
  const selectedEpisode = location.state?.data;

  return (
    <>
      <Header />
      <div className="container">
        <div className="episodeDetail-container">
          <PodcastCardDetail selectedId={id} />
          <EpisodeCardDetail selectedEpisode={selectedEpisode} />
        </div>
      </div>
    </>
  );
};

export default EpisodeDetail;
