import React, { useState } from "react";
import "../styles/PodcastCardDetail.css";
import "../styles/PodcastDetail.css";
import { useLocation, useParams } from "react-router-dom";
import Header from "../components/Header";
import PodcastCardDetail from "../components/PodcastCardDetail";
import EpisodesList from "../components/podcasts/EpisodesList";
import Loader from "../components/Loader";

const PodcastDetail = () => {
  const [episodesList, setEpisodesList] = useState([]);
  const [isLoadingEp, setisLoadingEp] = useState(true);
  const { id } = useParams();

  const location = useLocation();
  const selectedId = location.state?.data;

  return (
    <>
      {isLoadingEp && <Loader />}
      <Header />
      <div className="container">
        <div className="podcastDetail-container">
          <PodcastCardDetail selectedId={id} />
          <EpisodesList
            artistId={selectedId.id.attributes["im:id"]}
            episodesList={episodesList}
            setEpisodesList={setEpisodesList}
            setisLoadingEp={setisLoadingEp}
          ></EpisodesList>
        </div>
      </div>
    </>
  );
};

export default PodcastDetail;
