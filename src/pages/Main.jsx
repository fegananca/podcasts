import React, { useState } from "react";
import "../styles/Main.css";
import PodcastCard from "../components/main/PodcastCard";
import Header from "../components/Header";
import SearchBar from "../components/main/SearchBar";
import Loader from "../components/Loader";

const Main = () => {
  const [podcastList, setPodcastList] = useState([]);
  const [searchQuery, setSearchQuery] = useState(false);
  const [counter, setCounter] = useState(0);
  const [isLoadingList, setisLoadingList] = useState(true);

  return (
    <>
      {isLoadingList && <Loader />}
      <Header />
      <div className="container">
        <SearchBar
          setPodcastList={setPodcastList}
          setSearchQuery={setSearchQuery}
          setCounter={setCounter}
          counter={counter}
        ></SearchBar>
        <PodcastCard
          podcastList={podcastList}
          setPodcastList={setPodcastList}
          searchQuery={searchQuery}
          setCounter={setCounter}
          setisLoadingList={setisLoadingList}
        ></PodcastCard>
      </div>
    </>
  );
};

export default Main;
