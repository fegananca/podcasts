import { render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter as Router } from "react-router-dom";
import mockData from "../mockData.json";
import * as api from "../services/api";
import EpisodeDetail from "./EpisodeDetail";

jest.mock("../services/api", () => ({
  getPodcasts: jest.fn(),
  getEpisodes: jest.fn(),
}));

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    state: { data: mockData.episodes.results[1] },
    pathname: `/podcast/${mockData.podcasts[0].id.attributes["im:id"]}/episode/${mockData.episodes.results[1].collectionId}`,
  }),
}));

describe("episode detail page", () => {
  beforeEach(() => jest.clearAllMocks());

  test("should render selected episode", async () => {
    api.getPodcasts.mockResolvedValue(mockData.podcasts);
    api.getEpisodes.mockResolvedValue(mockData.episodes);

    render(
      <Router>
        <EpisodeDetail />
      </Router>
    );
    await waitFor(() => {
      expect(
        screen.getByText(mockData.episodes.results[1].trackName)
      ).toBeInTheDocument();
    });

    expect(screen.getByTestId("audio").getAttribute("src")).toBe(
      mockData.episodes.results[1].episodeUrl
    );
  });
});
